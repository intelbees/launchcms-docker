FROM ubuntu:14.04
MAINTAINER Khoa Tran  <tran.dang.khoa.khtn@gmail.com>
# Install PHP 5.6
RUN apt-get update 
RUN apt-get install -y software-properties-common
RUN apt-get install -y language-pack-en-base && export LC_ALL=en_US.UTF-8 && export LANG=en_US.UTF-8 && add-apt-repository -y ppa:ondrej/php5-5.6
RUN apt-get update && apt-get -y upgrade

# Update apt-get sources AND install MongoDB
# Install MongoDB
# add our user and group first to make sure their IDs get assigned consistently, regardless of whatever dependencies get added
RUN groupadd -r mongodb && useradd -r -g mongodb mongodb
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
RUN echo "deb http://repo.mongodb.com/apt/ubuntu trusty/mongodb-enterprise/stable multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-enterprise.list
RUN apt-get update
RUN apt-get install -y git
RUN mkdir -p /data/db/
RUN apt-get install -y mongodb-enterprise
RUN apt-get --assume-yes install php5-fpm php5 php5-cli php5-mcrypt php5-dev curl php-pear php5-dev php5-curl
RUN apt-get --assume-yes install pkg-config libssl-dev libsslcommon2-dev libpcre3-dev
RUN php5enmod mcrypt
RUN pecl install mongo
RUN pecl install mongodb
RUN echo "extension=mongo.so" >> /etc/php5/cli/php.ini
RUN echo "extension=mongodb.so" >> /etc/php5/cli/php.ini
RUN curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/bin/composer
RUN nohup sh -c mongod &

# Install MySQL
# add our user and group first to make sure their IDs get assigned consistently, regardless of whatever dependencies get added

RUN groupadd -r mysql && useradd -r -g mysql mysql
RUN apt-get  -y install software-properties-common
RUN echo 'mysql-server mysql-server/root_password password secret' | debconf-set-selections
RUN echo 'mysql-server mysql-server/root_password_again password secret' | debconf-set-selections
RUN apt-get -y install mysql-server





