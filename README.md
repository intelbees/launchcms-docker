# LaunchCMS Docker


This is the project for maintaining the docker box for running LaunchCMS tests

## Current configuration
PHP 5.6

MongoDB Enterprise Edition -  3.2

MongoDB Driver for PHP


## Contributing

Thank you for considering contributing to the LaunchCMS Docker !


## License

All sub projects of Launch CMS are open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
